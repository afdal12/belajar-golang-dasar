package main

import "fmt"

func main() {
	var nama string

	nama = "Afdal Afif Amran"
	fmt.Println(nama)

	nama = "Afdal Afif"
	fmt.Println(nama)

	var umur = 32
	fmt.Println(umur)

	gender := "laki-laki"
	fmt.Println(gender)

	var (
		firstName = "Afdal"
		lastName  = "Amran"
	)

	fmt.Println(firstName + " " + lastName)
}
